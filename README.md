## What should a personal website look like in a web-like metaverse?

This repo contains the result of a few hours of rough prototyping during the October 2021
[IndieWeb Create Day](https://events.indieweb.org/2021/10/indieweb-create-day-ZKw5v2nFDu6f).

The core ideas:

* Everybody sets up their own website, at their own domain (like the web used to be), but
  now in 3D

* Use open web standards (here: [A-Frame](https://aframe.io/), [Three.js](https://threejs.org),
  and [Mozilla Spoke](https://hubs.mozilla.com/spoke))

To run, clone the repo, and point your webserver to it. You need [Git **LFS**](https://git-lfs.github.com/)
because some of the files are quite large. The `space.glb` file was created from
[this Mozilla Spoke project](https://hubs.mozilla.com/spoke/projects/AHCoabT).

Here's a [demo video on YouTube](https://www.youtube.com/watch?v=PvUenrIIECU).
